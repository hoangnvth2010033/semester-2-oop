public class Client {

    public static void main(String[] args){
        Student studentA;
        Student studentB;

        studentA = new Student();
        studentB = new Student (1,"Nguyen A",true);
        studentB.printInfo();
        studentB.setName("Nguyen B");
        studentB.printInfo();
    }
    
}
