

public class Client {
    
    public Client(){
        //To do :
    }

    public void doUnchecked(String value){
        int result = canThrowUncheckedException(value);
        System.out.println("result = "+result);
    }

    private int canThrowUncheckedException(String value){
        return Integer.parseInt(value);
    }

    public void doChecked(){
        try {
            canThrowCheckedException();
            System.out.println("OK");
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
    private int canThrowCheckedException() throws Exception{
        throw new Exception("Failure");
    }
}

/* ANSWER QUESTION 
   1, 	Distinguishing unchecked Checked Exception and Exception?
   - Exception se thong bao loi tu phia client VD : loi code cua lap trinh vien, loi nhap du lieu cua nguoi dung,
      het bo nho, loi Internet ,...
   - Con Checked/ Unchecked exception se thong bao loi tu phia trinh bien dich (Compiler); da dc kiem duyet qua compiler

   2,   Using Checked Exception and use unchecked Exception?
   - Checked Exception da duoc trinh bien dich kiem duyet thanh cong; se bao loi chinh xac vi tri dong code cua ban
   - Unchecked Exception , khi ma trinh bien dich khong the nao kiem tra su sai lech luong du lieu ma ta ko luong den; Co the phat sinh
   loi khi den tay nguoi dung

   3, 	Why should not catch (Exception ex)?
    Boi mot phuong thuc chi nen bat mot ngoai le khi no co the xu ly no theo mot cach hop ly nao do.
    */