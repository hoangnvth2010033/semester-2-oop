

public class MoreClass {

    public static void main(String[] args){

            Object obj1 = new Object();
            System.out.println(obj1);

            Object obj2 = new Object(){
                public String toString(){
                    return "this is obj2";
                }
            };
            System.out.println(obj2);
        }
    
}
/* ANSWER 
   1, ✔	Compile and run the test class has written
   2, ✔	Why System.out.println(obj1); could be displayed?
      java.lang.Object@5aaa6d82
    3, ✔	What's the difference between two objects obj1 and obj2?
      obj2 tra ve kieu gia tri chuoi => ko bi loi
      obj1 ko tra ve gia tri gi va ko xac dinh dc =>  in loi */
