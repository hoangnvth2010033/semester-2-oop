import java.util.Scanner;


public class InvalidTriangleException {

    private double a;
    private double b;
    private double c;

    public InvalidTriangleException(double a, double b, double c){
                 this.a = a;
                 this.b = b;
                 this.c = c;
    }
    public void createTriangle(double a, double b, double c){

        try{
           
         Scanner input = new Scanner(System.in);
         System.out.print("\nNhap gia tri 3 canh tam giac a , b ,c :");
         a = input.nextDouble();
         b = input.nextDouble();
         c = input.nextDouble();

         if (a<=0 || b<=0 || c<=0 || a+b<c || b+c <a || a+c<b){

            throw new ArithmeticException(" The length of edges is invalided ! ");

         }

        }finally{
            System.out.println("Finish creation !");
        }
    }

    public void getArea(){
        double p = (a+b+c)/2.0;
        double S = Math.sqrt(p*(p-a) *(p-b) *(p-c));
        System.out.println("\n=> Dien tich tam giac la : "+S + " ( don vi dien tich )");
    }
   public void enterTriangle(){
       int n = 5;
       int a[] = new int[n];
       Scanner input = new Scanner(System.in);
       
       for(int i = 0;i<n;i++){
           System.out.printf("\nNhap thong so cho tam giac %d :",i+1);
           createTriangle(2, 3, 4);//Tham so tam thoi
           getArea(); 
       }
   }

   public static void main(String[] args){

       InvalidTriangleException triangle = new InvalidTriangleException(1, 2, 3);
       //Ham tao va tinh dien tich tam giac
       triangle.enterTriangle();

   }
    
}
