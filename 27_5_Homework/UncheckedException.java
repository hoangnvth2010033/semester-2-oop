

public class UncheckedException {


        public UncheckedException(){

        }

        public static void main(String[] args){

            int i, n=2;
            int a[] = new int[n];

            // Declare Scanner Object named input
            java.util.Scanner input = new java.util.Scanner(System.in);

            for(i = 0; i<n;i++){ // loi cu : for(i =0; i<=n ; i++)
                System.out.printf("a[%d] = ",i);
                a[i] = input.nextInt();
            }

        }
    
}

/* ANSWER 
  1, ✔	Compile and run the test class
  2, ✔	How many Exceptions may occur in the above code?
     1 : Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 2 out of bounds for length 2
  3, ✔	Please correct the above code to be able to catch every Exception that.
  */
