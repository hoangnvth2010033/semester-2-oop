

public class Account {
    
    // Declare a variable to store account
     private String accountNumber;
    private String accountHolderName;
     private String accountType;
    double balance;

    public Account(){
        System.out.println("Default Constructor Invoked...");
        accountNumber = " Not Specified ";
        accountHolderName = " Not Specified ";
        accountType = " Not Specified ";
        balance = 0.0;
    }

    public Account(String accountNumber, String accountHolderName, String accountType, double balance){
        System.out.println("Parameterized Constructor invoked");
        this.accountNumber = accountNumber;
        this.accountHolderName = accountHolderName;
        this.accountType = accountType;
        this.balance = balance;
    }

    public String getAccountNumber(){
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber){
        this.accountNumber = accountNumber;
    }

    public String getAccountHolderName(){
        return accountHolderName;
    }

  

}
